//
//  ViewController.swift
//  DataPickerExample
//
//  Created by Diego Caridei on 08/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func clickButton(sender: AnyObject) {
        let date: NSDate = datePicker.date
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "EEEE"
        label.text = formatter.stringFromDate(date)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
  
    }


}

