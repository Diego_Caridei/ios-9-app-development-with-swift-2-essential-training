//
//  ViewController.swift
//  SwitchAndSegmetExample
//
//  Created by Diego Caridei on 09/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func segmentChange(sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        let title : String = sender.titleForSegmentAtIndex(index)!
        print ("Selected item is \(title)")
        
        
    }
    @IBAction func switchChanger(sender: UISwitch) {
        if  sender.on {
            indicatorView.startAnimating()
        }else{
            indicatorView.stopAnimating()
        }
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

