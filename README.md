# iOS 9 App Development with Swift2
Learn how to create native apps for iOS 9, the latest operating system for the iPhone and iPad. It all starts with the installation of Xcode, the free all-in-one development environment built by Apple. Author Todd Perkins then shows how to select an app template and start working on the core iOS programming skills: creating interactions; responding to touch events, button clicks, and text input; and using delegation. He also spends a few chapters on iOS interface design. In these sections, you'll learn how to use Interface Builder to visually design your app's UI and use size classes to responsively adjust to different screen sizes. Plus, find out how to use views to display images, web content, and table data, and transition seamlessly between views in multiview apps.

Interested in developing for Apple Watch or Apple TV? Get a sneak peek in Chapter 9. The course wraps up with a look at the submission process for the App Store.
Topics include:
Installing Xcode and the iOS SDK
Creating a simple iOS app
Creating a basic interaction with a button
Choosing an object as a first responder
Creating a user interface with Interface Builder and Auto Layout
Working with sliders and progress bars
Creating views to show images and web pages
Developing responsive layouts with size classes
Understanding the iOS architecture
Loading data in table views
Transitioning between views
Developing for the iPad, Apple Watch, and Apple TV
Submitting apps to the App Store
