//
//  InterfaceController.swift
//  WatchExample WatchKit Extension
//
//  Created by Diego Caridei on 10/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    
    @IBAction func tapButton() {
        print ("Button tapped")
    }
    
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
