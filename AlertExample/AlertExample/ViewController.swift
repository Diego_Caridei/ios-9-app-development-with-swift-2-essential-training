//
//  ViewController.swift
//  AlertExample
//
//  Created by Diego Caridei on 08/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func buttonClick(sender: AnyObject) {
        let alert = UIAlertController(title: "Alert", message: "Hello Alert", preferredStyle: .Alert)
        let action = UIAlertAction(title: "Close", style: .Default) { (a:UIAlertAction) in
            print("Item Selected")
        }
        
        let action1 = UIAlertAction(title: "Item1", style: .Default) { (a:UIAlertAction) in
            print("Item 1 Selected")
        }
        
        alert.addAction(action)
        alert.addAction(action1)
        
        self.presentViewController(alert, animated: true) { 
            print("Alert presented")
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

