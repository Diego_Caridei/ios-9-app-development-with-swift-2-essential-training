//
//  DetailController.swift
//  SendDataExample
//
//  Created by Diego Caridei on 10/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class DetailController: UIViewController {

    var text : String = ""
    
    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = text
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
