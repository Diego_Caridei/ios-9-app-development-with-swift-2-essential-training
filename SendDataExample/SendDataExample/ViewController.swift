//
//  ViewController.swift
//  SendDataExample
//
//  Created by Diego Caridei on 10/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detail : DetailController = segue.destinationViewController as! DetailController
        
        if segue.identifier == "optionA"{
            detail.text = "optionA"
        }
        else if segue.identifier == "optionB"{
            detail.text = "optionB"
        }
        else{
            detail.text = "optionC"
        }
    }
}

