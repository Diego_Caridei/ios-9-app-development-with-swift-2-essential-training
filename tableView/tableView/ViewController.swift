//
//  ViewController.swift
//  tableView
//
//  Created by Diego Caridei on 09/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let array : [String] = ["Mela","Pera","Banana"]
    let nomi : [String] = ["Diego","Nicola","Salvatore","Giuseppe"]
    let customCell : CustomCell = CustomCell()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return array.count
        }
            
        else {
            return nomi.count

        }
        
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Frutta"
        }else{
            return "Nomi"
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomCell
        
        if indexPath.section == 0 {
            cell.label.text = array[indexPath.row]
           // cell.label.text = "Subtitle 1"
        }else{
            cell.label.text = nomi[indexPath.row]
           // cell.label.text = "Subtitle 2"

        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0{
        print ("Hai selezionato:\(array[indexPath.row])")
        }else{
            print ("Hai selezionato:\(nomi[indexPath.row])")

        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

