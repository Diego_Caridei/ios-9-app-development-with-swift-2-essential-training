//
//  ViewController.swift
//  SliderAndProgressBarExample
//
//  Created by Diego Caridei on 08/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var progressBar: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func sliderDidChange(sender: UISlider) {
        let percent : Float = sender.value / sender.maximumValue
        progressBar.progress = percent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

