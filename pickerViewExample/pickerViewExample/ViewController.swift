//
//  ViewController.swift
//  pickerViewExample
//
//  Created by Diego Caridei on 08/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    let days :[String] =  ["Sunday","Monday","Tuesday","Wednesday",
                            "Thursday","Fraiday","Saturday"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return days.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return days[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print("\(days[row])")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

